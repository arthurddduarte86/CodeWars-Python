# Readme com informações e catálogo do meu processo na plataforma CodeWars para Python

## Sobre
Sejam bem vindos, este é o catálogo de exercícios e códigos para a linguagem Python na plataforma CodeWars.

Welcome, here you find code challenges for Python language from CodeWars.com

# Kata
## Rank:8 kyu  Rank:7 kyu  Rank:6 kyu   
**Total**: +65

* `Multiply` || [  multiply.py  ](https://github.com/arthurddduarte86/CodeWars-Py/blob/main/Code-Py/Multiply.py)  Site [CodeWars, py  ](https://www.codewars.com/kata/50654ddff44f800200000004/train/python)
* `Sentence Smash` || [  Sentence_Smash.py  ](https://github.com/arthurddduarte86/CodeWars/blob/main/Code-Py/Sentence_Smash.py)  Site [CodeWars, py  ](https://www.codewars.com/kata/53dc23c68a0c93699800041d/train/python)
* `Reversed Strings` || [  Reversed_Strings.py  ](https://github.com/arthurddduarte86/CodeWars/blob/main/Code-Py/Reversed_Strings.py)  Site [CodeWars, py  ](https://www.codewars.com/kata/5168bb5dfe9a00b126000018/train/python)
* `How good are you really?` || [  How_good_are_you_really.py  ](https://github.com/arthurddduarte86/CodeWars/blob/main/Code-Py/How_good_are_you_really.py)  Site [CodeWars, py  ](https://www.codewars.com/kata/5601409514fc93442500010b/train/python)
* `Sum of positive` || [  Sum_of_positive.py  ](https://github.com/arthurddduarte86/CodeWars/blob/main/Code-Py/Sum_of_Positive.py)  Site [CodeWars, py  ](https://www.codewars.com/kata/5715eaedb436cf5606000381/train/python)
* `Hello World` || [  hello-world.py  ](https://github.com/arthurddduarte86/CodeWars-Python/blob/main/Code-Py/Hello-World.py)  Site [CodeWars, py  ](https://www.codewars.com/kata/523b4ff7adca849afe000035/train/python)
* `Square(n) Sum` || [  Square(n)_Sum.py  ](https://github.com/arthurddduarte86/CodeWars-Python/blob/main/Code-Py/Square(n)_Sum.py)  Site [CodeWars, py  ](https://www.codewars.com/kata/515e271a311df0350d00000f/train/python)
* `Beginner Series #4 Cockroach` || [  Beginner_Series_#4_Cockroach.py  ](https://github.com/arthurddduarte86/CodeWars-Python/blob/main/Code-Py/Beginner_Series_%234_Cockroach.py)  Site [CodeWars, py  ](https://www.codewars.com/kata/55fab1ffda3e2e44f00000c6/train/python)
* `Removing Elements` || [  Removing_Elements.py  ](https://github.com/arthurddduarte86/CodeWars-Python/blob/main/Code-Py/Removing_Elements.py)  Site [CodeWars, py  ](https://www.codewars.com/kata/5769b3802ae6f8e4890009d2/train/python)
* `MakeUpperCase` || [  MakeUpperCase.py  ](https://github.com/arthurddduarte86/CodeWars-Python/blob/main/Code-Py/MakeUpperCase.py)  Site [CodeWars, py  ](https://www.codewars.com/kata/57a0556c7cb1f31ab3000ad7/train/python)
* `Calculate BMI` || [  Calculate_BMI.py  ](https://github.com/arthurddduarte86/CodeWars-Python/blob/main/Code-Py/Calculate_BMI.py)  Site [CodeWars, py  ](https://www.codewars.com/kata/57a429e253ba3381850000fb/train/python)
* `Switch it Up!` || [  Switch_it_Up.py  ](https://github.com/arthurddduarte86/CodeWars-Python/blob/main/Code-Py/Switch_it_Up.py)  Site [CodeWars, py  ](https://www.codewars.com/kata/5808dcb8f0ed42ae34000031/train/python)
* `Keep Hydrated!` || [  Keep_Hydrated.py  ](Code-Py/Keep_Hydrated.py)  Site [CodeWars, py  ](https://www.codewars.com/kata/582cb0224e56e068d800003c/train/python)
* `Convert boolean values to strings 'Yes' or 'No'` || [  `Convert_boolean_values_to_strings.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/53369039d7ab3ac506000467/train/python)
* `Even or Odd` || [  `Even_or_Odd.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/53da3dbb4a5168369a0000fe/train/python)
* `Grasshopper - Messi goals function` || [  `Grasshopper-Messi_goals_function.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/55f73be6e12baaa5900000d4/train/python)
* `A Needle in the Haystack` || [  `A_Needle_in_the_Haystack.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/56676e8fabd2d1ff3000000c/train/python)
* `Returning Strings` || [  `Returning_Strings.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/55a70521798b14d4750000a4/train/python)
* `Remove exclamation marks` || [  `Remove_exclamation_marks.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/57a0885cbb9944e24c00008e/train/python)
* `Reversed Words` || [  `reversed_words.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/51c8991dee245d7ddf00000e/train/python)
* `Jaden Casing Strings` || [  `Jaden_Casing_Strings.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/5390bac347d09b7da40006f6/train/python)
* `Counting sheep...` || [  `Counting_sheep.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/54edbc7200b811e956000556/train/python)
* `Mumbling` || [  `Mumbling.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/5667e8f4e3f572a8f2000039/train/python)
* `Testing 1-2-3` || [  `Testing_123.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/54bf85e3d5b56c7a05000cf9/train/python)
* `Vowel Count` || [  `vowel_count.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/54ff3102c1bad923760001f3/train/python)
* `Complementary DNA` || [  `Complementary_DNA.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/554e4a2f232cdd87d9000038/train/python)
* `Square Every Digit` || [  `Square_Every_Digit.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/546e2562b03326a88e000020/train/python)
* `Binary Addition` || [  `Binary_Addition.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/551f37452ff852b7bd000139/train/python)
* `Grasshopper - Grade book` || [  `Grasshopper-Grade_book.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/55cbd4ba903825f7970000f5/train/python)
* `Fake Binary` || [  `fake_binary.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/57eae65a4321032ce000002d/train/python)
* `List Filtering` || [  `List_Filtering.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/53dbd5315a3c69eed20002dd/train/python)
* `Parse nice int from char problem` || [  `Parse_nice_int_from_char_problem.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/557cd6882bfa3c8a9f0000c1/train/python)
* `altERnaTIng cAsE <=> ALTerNAtiNG CaSe` || [  `altERnaTIng_cAsE_ALTerNAtiNG_CaSe.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/56efc695740d30f963000557/train/python)
* `Convert number to reversed array of digits` || [`Convert_number_to_reversed_array_of_digits.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/5583090cbe83f4fd8c000051/train/python)
* `Find the next perfect square!` || [`Find_the_next_perfect_square.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/56269eb78ad2e4ced1000013/train/python)
* `Ones and Zeros` || [`Ones_and_Zeros.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/578553c3a1b8d5c40300037c/train/python)
* `Is he gonna survive?` || [`Is_he_gonna_survive.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/59ca8246d751df55cc00014c/train/python)
* `Opposites Attract` || [`Opposites_Attract.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/555086d53eac039a2a000083/train/python)
* `Beginner Series #2 Clock` || [`Beginner_Series_2_Clock.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/55f9bca8ecaa9eac7100004a/train/python)
* `DNA to RNA Conversion` || [`DNA_to_RNA_Conversion.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/5556282156230d0e5e000089/train/python)
* `Find the odd int` || [`Find_the_odd_int.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/54da5a58ea159efa38000836/train/python)
* `You're a square!` || [`You're_a_square.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/54c27a33fb7da0db0100040e/train/python)
* `RNA to Protein Sequence Translation` || [`RNA_to_Protein_Sequence_Translation.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/555a03f259e2d1788c000077/train/python)
* `Convert a String to a Number!` || [`Convert_a_String_to_a_Number.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/544675c6f971f7399a000e79/train/python)
* `Remove First and Last Character` || [`Remove_First_and_Last_Character.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/56bc28ad5bdaeb48760009b0/train/python)
* `Sum Arrays` || [`Sum_Arrays.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/53dc54212259ed3d4f00071c/train/python)
* `Return Negative` || [`Return_Negative.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/53dc54212259ed3d4f00071c/train/python)
* `Delete occurrences of an element if it occurs more than n times` || [`Delete_occurrences_of_an_element_if_it_occurs_more_than_n_times.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/554ca54ffa7d91b236000023/train/python)
* `Count of positives / sum of negatives` || [`Count_of_positives_sum_of_negatives.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/576bb71bbbcf0951d5000044/train/python)
* `Will you make it?` || [`Will_you_make_it.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/5861d28f124b35723e00005e/train/python)
* `Reversed sequence` || [`Reversed_sequence.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/5a00e05cc374cb34d100000d/train/python)
* `Who likes it?` || [`Who_likes_it.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/5266876b8f4bf2da9b000362/train/python)
* `Categorize New Member` || [`Categorize_New_Member.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/5502c9e7b3216ec63c0001aa/train/python)
* `Tribonacci Sequence` || [`Tribonacci_Sequence.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/556deca17c58da83c00002db/train/python)
* `Abbreviate a Two Word Name` || [`Abbreviate_a_Two_Word_Name.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/57eadb7ecd143f4c9c0000a3/train/python)
* `Bouncing Balls` || [`Bouncing_Balls.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/5544c7a5cb454edb3c000047/train/python)
* `Friend or Foe?` || [`Friend_or_Foe.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/55b42574ff091733d900002f/train/python)
* `Find the unique number` || [`Find_the_unique_number.py`  ] Site [CodeWars, py  ](https://www.codewars.com/kata/585d7d5adb20cf33cb000235/train/python)
